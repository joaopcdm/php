<?php

$dir1 = 'folder_01';
$dir2 = 'folder_02';

if (!is_dir($dir1)) mkdir($dir1);
if (!is_dir($dir2)) mkdir($dir2);

$filename = 'README.md';

if (file_exists($dir1 . DIRECTORY_SEPARATOR . $filename)) {

    $file = fopen($dir1 . DIRECTORY_SEPARATOR . $filename, 'w+');
    fwrite($file, date('d/m/Y H:i:s'));
    fclose($file);

}

if (file_exists($dir1 . DIRECTORY_SEPARATOR . $filename)) {

    rename(
        $dir1 . DIRECTORY_SEPARATOR . $filename, # Origem
        $dir2 . DIRECTORY_SEPARATOR . $filename  # Destino
    );
    echo "Arquivo movido de $dir1 para $dir2!";

} else {

    rename(
        $dir2 . DIRECTORY_SEPARATOR . $filename, # Origem
        $dir1 . DIRECTORY_SEPARATOR . $filename  # Destino
    );
    echo "Arquivo movido de $dir2 para $dir1!";

}

?>