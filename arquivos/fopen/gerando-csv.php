<?php

require_once 'config.php';

$sql = new Sql();

$usuarios = $sql->select("SELECT * FROM tb_usuarios ORDER BY idusuario");

$columns = array();

foreach ($usuarios[0] as $key => $value) {
    array_push($columns, ucfirst($key));
}

$file = fopen('usuarios.csv', 'w+');

fwrite($file, implode(', ', $columns) . "\r\n");

foreach ($usuarios as $row) {

    $data = array();

    foreach ($row as $key => $value) {
        array_push($data, $value);
    }

    fwrite($file, implode(', ', $data) . "\r\n");

}

fclose($file);

?>