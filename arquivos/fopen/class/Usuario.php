<?php

class Usuario {

    private $idusuario;
    private $deslogin;
    private $dessenha;
    private $dtcadastro;

    # Getters
    public function getIdusuario() {
        return $this->idusuario;
    }

    public function getDeslogin() {
        return $this->deslogin;
    }

    public function getDessenha() {
        return $this->dessenha;
    }

    public function getDtcadastro() {
        return $this->dtcadastro;
    }

    # Setters
    public function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }

    public function setDeslogin($deslogin) {
        $this->deslogin = $deslogin;
    }

    public function setDessenha($dessenha) {
        $this->dessenha = $dessenha;
    }

    public function setDtcadastro($dtcadastro) {
        $this->dtcadastro = $dtcadastro;
    }

    public function setData($data) {
        $this->setIdusuario($data['idusuario']);
        $this->setDeslogin($data['deslogin']);
        $this->setDessenha($data['dessenha']);
        $this->setDtcadastro(new DateTime($data['dtcadastro']));
    }

    # Retorna o resultado de acordo com o ID passado por parâmetro
    public function loadById($id) {
        $sql = new Sql();
        $results = $sql->select('SELECT * FROM tb_usuarios WHERE idusuario = :ID', array(
            ':ID' => $id
        ));

        if (isset($results[0])) {
            $this->setData($results[0]);
        }

    }

    # Retorna todos os resultados
    public static function getList() {
        $sql = new Sql();
        return $sql->select('SELECT * FROM tb_usuarios ORDER BY deslogin;');
    }

    # Retorna os resultados de acordo com a pesquisa
    public static function search($login) {
        $sql = new Sql();
        return $sql->select('SELECT * FROM tb_usuarios WHERE deslogin LIKE :SEARCH ORDER BY deslogin', array(
            ':SEARCH' => "%$login%"
        ));
    }

    # Retorna os resultados de acordo com o usuário e senha passados
    public function login($login, $password) {
        $sql = new Sql();
        $results = $sql->select('SELECT * FROM tb_usuarios WHERE deslogin = :LOGIN AND dessenha = :PASSWORD', array(
            ':LOGIN' => $login,
            ':PASSWORD' => $password
        ));

        if (isset($results[0])) {
            $this->setData($results[0]);
        } else {
            throw new Exception('Login e/ou senha inválidos.');
        }
    }

    # Faz o insert de um novo usuário
    public function insert() {
        $sql = new Sql();
        $results = $sql->select('CALL sp_usuarios_insert(:LOGIN, :PASSWORD)', array(
            ':LOGIN' => $this->getDeslogin(),
            ':PASSWORD' => $this->getDessenha()
        ));

        if (count($results) > 0) {
            $this->setData($results[0]);
        }
    }

    # Faz o update de um usuário
    public function update($login, $password) {
        $this->setDeslogin($login);
        $this->setDessenha($password);

        $sql = new Sql();
        $sql->query('UPDATE tb_usuarios SET deslogin = :LOGIN, dessenha = :PASSWORD WHERE idusuario = :ID', array(
            ':LOGIN' => $this->getDeslogin(),
            ':PASSWORD' => $this->getDessenha(),
            ':ID' => $this->getIdusuario()
        ));
    }

    # Faz o delete de um usuário
    public function delete() {
        $sql = new Sql();
        $sql->query('DELETE FROM tb_usuarios WHERE idusuario = :ID', array(
            ':ID' => $this->getIdusuario()
        ));

        $this->setIdusuario(0);
        $this->setDeslogin('');
        $this->setDessenha('');
        $this->setDtcadastro(new DateTime());
    }

    # Métodos mágicos
    public function __construct($login = '', $password = '') {
        $this->setDeslogin($login);
        $this->setDessenha($password);
    }

    public function __toString() {
        return json_encode(array(
            'idusuario' => $this->getIdusuario(),
            'deslogin' => $this->getDeslogin(),
            'dessenha' => $this->getDessenha(),
            'dtcadastro' => $this->getDtcadastro()->format('d/m/Y H:i:s')
        ));
    }

}

?>