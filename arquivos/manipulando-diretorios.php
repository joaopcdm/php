<?php

$dirname = 'images';

if (!is_dir($dirname)) {

    # Cria um diretório
    mkdir($dirname);
    echo "Diretório /$dirname criado com sucesso!";

} else {

    # Remove um diretório
    rmdir($dirname);
    echo "Diretório /$dirname foi removido!";

}

?>