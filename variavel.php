<?php
    $nomeCompleto = 'João Pedro da Cruz Melo';
    echo 'Você está vendo a variável porque ela ainda não é indefinida, aqui está o valor dela: <b>' .$nomeCompleto . '</b>. Agora vamos deixá-la indefinida...<br>';
    unset($nomeCompleto);
    
    if(isset($nomeCompleto)){
        echo $nomeCompleto;
    } else{
        echo '<b>Variável é indefinida!</b>';
    }
?>