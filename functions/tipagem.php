<?php

function soma(int ...$valores) { # Isso significa que terão infinitos parâmetros do tipo Int
    return array_sum($valores);
}

echo soma(2, 2) . '<br>';
echo soma(8, 7, 9) . '<br>';
echo soma(10, 25, 50) . '<br>';

function somaReturnType(int ...$valores):string { # Define o tipo do retorno
    return array_sum($valores);
}

echo var_dump(somaReturnType(2, 2, 5));

?>