<?php

# Esta é uma função de sub-rotina, pois não retorna algo
function subRotina() {
    echo 'Esta é uma função de sub-rotina! <br>';
}

# Esta é uma função propriamente dita, pois retorna algo
function funcao() {
    return 'Esta é uma função propriamente dita!';
}

subRotina();
echo funcao();

?>