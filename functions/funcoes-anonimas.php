<?php

function teste($callback) {
    # Processo lento
    $callback();
}

teste(function() {
    echo 'O processo lento foi finalizado! <br>';
});


$fn = function($a) {
    var_dump($a);
};

$fn('Olá');

?>