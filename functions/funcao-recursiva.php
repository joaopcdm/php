<?php

$hierarquia = array(
    array(
        'nome_cargo' => 'CEO',
        'subordinados' => array(
            # Diretor Comercial
            array(
                'nome_cargo' => 'Diretor Comercial',
                'subordinados' => array(
                    # Gerente de Vendas
                    array(
                        'nome_cargo' => 'Gerente de Vendas'
                    )
                )
            ),
            # Diretor Financeiro
            array(
                'nome_cargo' => 'Diretor Financeiro',
                'subordinados' => array(
                    # Gerente de Contas a Pagar
                    array(
                        'nome_cargo' => 'Gerente de Contas a Pagar',
                        'subordinados' => array(
                            # Supervisor de Pagamentos
                            array(
                                'nome_cargo' => 'Supervisor de Pagamentos'
                            )
                        )
                    ),
                    # Gerente de Compras
                    array(
                        'nome_cargo' => 'Gerente de Compras',
                        'subordinados' => array(
                            # Supervisor de Suprimentos
                            array(
                                'nome_cargo' => 'Supervisor de Suplimentos',
                                'subordinados' => array(
                                    # Funcionários
                                    array(
                                        'nome_cargo' => 'Funcionários'
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    )
);

function exibe($cargos) {
    $html = '<ul>';
    
    foreach ($cargos as $cargo) {
        $html .= '<li>';
        $html .= $cargo['nome_cargo'];

        if (isset($cargo['subordinados']) && count($cargo['subordinados']) > 0) {
            $html .= exibe($cargo['subordinados']);
        }

        $html .= '</li>';
    }

    $html .= '</ul>';

    return $html;
}

echo exibe($hierarquia);

?>