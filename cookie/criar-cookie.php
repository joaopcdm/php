<?php

$username = 'joao.melo';
$data = array(
    'username' => $username,
    'token' => md5(date('d/m/Y H:i:s') . $username)
);

if (isset($_COOKIE['USER_DETAILS'])) {
    setcookie('USER_DETAILS');
    echo 'Cookie já existia e foi deletado! Recarregue a página para criá-lo novamente.';
} else {
    setcookie('USER_DETAILS', json_encode($data), time() + 3600);
    echo 'Cookie criado com sucesso!';
}

?>