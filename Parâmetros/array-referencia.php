<?php

$pessoas = array(
    'nome' => 'João',
    'idade' => 15
);

foreach ($pessoas as &$pessoa) {
    if (gettype($pessoa) === 'integer') $pessoa += 10;
    echo $pessoa . '<br>';
}

print_r($pessoas);

?>