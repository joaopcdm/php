<?php

$a = 10;

function trocaValor($parametro) {
    $parametro += 20;
    return $parametro;
}

echo 'Valor:<br>';
echo $a . '<br>';
echo trocaValor($a) . '<br>';
echo $a . '<br>';

function trocaValorRef(&$parametro) { # Com &, você pode alterar o valor da variável
    $parametro += 20;
    return $parametro;
}

echo 'Referência:<br>';
echo $a . '<br>';
echo trocaValorRef($a) . '<br>';
echo $a;

?>