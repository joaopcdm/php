<?php

function helloWorld($nome = 'mundo', $dia = 'Domingo') {
    return "Olá, $nome! Hoje é $dia!<br>";
}

echo helloWorld();
echo helloWorld('João', 'Segunda-Feira');

?>