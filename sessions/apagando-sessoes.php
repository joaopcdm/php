<?php

require_once 'config.php';

session_unset(); # Apaga todas as sessões na aplicação

if (!isset($_SESSION['usuario'])) {
    echo 'Sessão apagada com sucesso!';
}

?>