<?php

require_once 'config.php';

# session_status(); // Mostra o status da sua sessão atual

switch (session_status()) {
    case PHP_SESSION_DISABLED: # 0
    echo 'Sessões desabilitadas';
    break;

    case PHP_SESSION_NONE: # 1
    echo 'Sessões habilitadas, mas não foram criadas ainda';
    break;

    case PHP_SESSION_ACTIVE: # 2
    echo 'Sessões habilitadas e existe uma criada';
    break;
}

?>