<?php

$a = 55.0;
$b = 55;

var_dump($a > $b);
echo '<br>';
var_dump($a < $b);
echo '<br>';
var_dump($a == $b); # Comparação de valor
echo '<br>';
var_dump($a === $b); # Comparação de valor e tipo
echo '<br>';
var_dump($a != $b); # Valor diferente
echo '<br>';
var_dump($a !== $b); # Valor e tipo diferente

?>