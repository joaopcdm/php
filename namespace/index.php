<?php

# Configurações globais
require_once 'config.php';

# Usar a classe Cadastro dentro do namespace Cliente
use Cliente\Cadastro;

# Instância a classe
$obj = new Cadastro();
$obj->setNome('Joao Melo');
$obj->setEmail('joao.pedro6532@gmail.com');
$obj->setSenha('123456');

$obj->registrarVenda();

echo '<br>';

# __toString()
echo $obj;

?>