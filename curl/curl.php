<?php

$cep = '03917050';
$link = "https://viacep.com.br/ws/$cep/json";

$ch = curl_init($link);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); # Requer uma resposta
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); # Indica que não é preciso um SSL

$response = curl_exec($ch);

$data = json_decode($response, true);
print_r($data);

?>