<?php
    $nome = 'João';
    $sobrenome = 'Melo';
    $site = 'www.grupoccompany.com.br';
    $ano = 2002;
    $salario = 550.00;
    $bloqueado = false;
    $coresPreferidas = array("Azul", "Preto", "Verde", "Vermelho");
    $curriculo = fopen('tiposdedados.php', 'r'); # fopen() é uma função para abrir arquivos, onde o primeiro parâmetro é o nome do arquivo com a extensão e o segundo parâmetro é o tipo de leitura.

    echo 'Nome: ' . $nome . ' ' . $sobrenome . '<br>';
    echo 'Site: ' . $site . '<br>';
    echo 'Ano de nascimento: ' . $ano . '<br>';
    echo 'Salário mensal: ' . $salario . '<br>';
        if($bloqueado == true){
            $status = 'Bloqueado';
        } else{
            $status = 'Disponível';
        }
    echo 'Status: ' . $status. '<br>';
    echo 'Cor preferida: ' . $coresPreferidas[0] . '<br>';
?>