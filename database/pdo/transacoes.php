<?php

# commit - Confirmação
# rollback - Cancelamento

$conn = new PDO('mysql:host=localhost;dbname=dbphp7', 'root', '');

# Transação
$conn->beginTransaction();

$stmt = $conn->prepare("DELETE from tb_usuarios WHERE idusuario = ?");

$id = 1;

if ($stmt->execute(array($id))) {
    echo 'Deletado OK <br>';
}

/*# Cancelar operação
if ($conn->rollback()) {
    echo 'rollback() efetuado com sucesso!';
}*/

# Confirmar operação
if ($conn->commit()) {
    echo 'commit() efetuado com sucesso!';
}

?>