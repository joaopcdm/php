<?php

# Conexão com o banco de dados
$conn = new PDO('mysql:dbname=dbphp7;host=localhost', 'root', '');

# Query
$stmt = $conn->prepare('select * from tb_usuarios order by deslogin');

# Executa a query
$stmt->execute();
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($results);

?>