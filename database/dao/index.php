<?php

require_once 'config.php';

# Carrega um usuário de acordo com o ID
#$user = new Usuario();
#$user->loadById(1);
#echo $user;

# Carrega uma lista de usuários
#$list = Usuario::getList();
#echo json_encode($list);

# Carrega uma lista de usuários buscando pelo login
#$search = Usuario::search('joao');
#echo json_encode($search);

# Carrega um usuário usando o login e a senha
#$usuario = new Usuario();
#$usuario->login('joao.melo', 'jm#senha');
#echo $usuario;

# Insert de um usuário novo
#$cliente = new Usuario('joao.melo', 'jm#senha');
#$cliente->insert();
#echo $cliente;

# Update de um usuário
#$usuario = new Usuario();
#$usuario->loadById(2);
#$usuario->update('joao.melo', 'jm#senha');
#echo $usuario;

# Delete de um usuário
#$usuario = new Usuario();
#$usuario->loadById(5);
#$usuario->delete();
#echo $usuario;

?>