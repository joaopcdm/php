<?php

class Car {

    private $model;
    private $engine;
    private $year;

    # Getters
    public function getModel() {
        return $this->model;
    }

    public function getEngine():float {
        return $this->engine;
    }

    public function getYear():int {
        return $this->year;
    }

    public function getAll() {
        return array(
            'model' => $this->getModel(),
            'engine' => $this->getEngine(),
            'year' => $this->getYear()
        );
    }

    # Setters
    public function setModel($model) {
        $this->model = $model;
    }

    public function setEngine($engine) {
        $this->engine = $engine;
    }

    public function setYear($year) {
        $this->year = $year;
    }

}

# Instância e definições de valores por via de setters
$car = new Car();
$car->setModel('Gol GT');
$car->setEngine('1.6');
$car->setYear('2017');

# Exibe o resultado do método getter
var_dump($car->getAll());

?>