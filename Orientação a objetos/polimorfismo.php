<?php

abstract class Animal {
    public function falar() {
        return 'Som';
    }

    public function mover() {
        return 'Anda';
    }
}

class Cachorro extends Animal {
    public function falar() {
        return 'Late';
    }
}

class Gato extends Animal {
    public function falar() {
        return 'Mia';
    }
}

class Passaro extends Animal {
    public function falar() {
        return 'Canta';
    }

    public function mover() {
        return 'Voa e ' . parent::mover();
    }
}

$obj = new Cachorro();
echo $obj->falar() . '<br>';
echo $obj->mover() . '<br>';

echo '---------------<br>';

$obj = new Gato();
echo $obj->falar() . '<br>';
echo $obj->mover() . '<br>';

echo '---------------<br>';

$obj = new Passaro();
echo $obj->falar() . '<br>';
echo $obj->mover() . '<br>';

?>