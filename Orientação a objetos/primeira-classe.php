<?php

class Pessoa {

    public $nome; # Criação do atributo

    public function falar() { # Criação do método
        return "O meu nome é $this->nome!";
    }

}

# Instância
$pessoa = new Pessoa();
$pessoa->nome = 'João Melo';
echo $pessoa->falar();

?>