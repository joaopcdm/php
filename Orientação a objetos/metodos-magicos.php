<?php

class Endereco {

    private $logradouro;
    private $numero;
    private $cidade;

    # Método mágico
    public function __construct($logradouro, $numero, $cidade) {
        $this->logradouro = $logradouro;
        $this->numero = $numero;
        $this->cidade = $cidade;
    }

    # Instrui a maneira correta de dar um echo na variável $endereco
    public function __toString() {
        return $this->logradouro . ', ' . $this->numero . ', ' . $this->cidade . '<br>';
    }

    # Chamado no final dos processos
    public function __destruct() {
        var_dump('Destruir');
    }

}

$endereco = new Endereco('Rua Caetano Pero Neto', '141', 'São Paulo');
# var_dump($endereco);

# Chama o método __toString()
echo $endereco;

# Chama o método __destruct()
unset($endereco);

?>