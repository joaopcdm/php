<?php

class Pessoa {

    public $nome = 'João Melo'; # Todos podem ver
    protected $idade = 15; # Somente métodos dentro da classe
    private $senha = '123456'; # Herdeiros da classe não podem acessar

    public function verDados() {
        return array(
            'nome' => $this->nome,
            'idade' => $this->idade,
            'senha' => $this->senha
        );
    }

}

$obj = new Pessoa();

//echo $obj->nome . '<br>'; # Public
//echo $obj->idade . '<br>'; # Protected
//echo $obj->senha . '<br>'; # Private

var_dump($obj->verDados());

?>