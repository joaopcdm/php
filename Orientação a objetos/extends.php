<?php

class Pessoa {

    public $nome = 'João Melo'; # Todos podem ver
    protected $idade = 15; # Somente métodos dentro da classe e herdeiros
    private $senha = '123456'; # Somente visível dentro da classe

    public function verDados() {
        return array(
            'nome' => $this->nome,
            'idade' => $this->idade,
            'senha' => $this->senha
        );
    }

}

class Programador extends Pessoa {

    public function verDados() {
        # Mostra de qual classe vem o objeto recuperado
        echo get_class($this) . '<br>';

        return array(
            'nome' => $this->nome,
            'idade' => $this->idade,
            'senha' => $this->senha
        );
    }

}

$obj = new Programador();

var_dump($obj->verDados());

?>