<?php

$idade = 95;
$idadeCrianca = 12;
$idadeMaior = 18;
$idadeIdoso = 65;

if ($idade < $idadeCrianca) {
    echo 'Você é uma criança!';
} elseif ($idade < $idadeMaior) {
    echo 'Você é um adolescente!';
} elseif ($idade < $idadeIdoso) {
    echo 'Você é um adulto!';
} elseif ($idade > 110) {
    echo 'Você está muito velho!';
} else {
    echo 'Você é um idoso!';
}

echo '<br>';
echo ($idade < $idadeMaior) ? 'Menor de idade' : 'Maior de idade';

?>